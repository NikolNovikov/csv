'''
1. open the given file in 'read' mode
2. pass the loaded CSV to a function that would:
  a. create new list of tuples for the employees above 32 as (name, department).
  b. create new list of lists (csv representation) with the new locations, based on index.
  c. return both lists.
3. print to the terminal the output of the people.
4. write to a new file the modified data.
'''
import csv

def handle_data(data: list) -> tuple[list[tuple[str, str]], list[list]]:

    locations = ['Location','Tel Aviv', 'Paris', 'New York', 'Singapore' , 'London','Haifa']
    older_then_32 = []
    modified_data = []
    for i, line in enumerate(data):
        # enumearting to control the index for the locations list
        name, age, dep, loc = line
        try:
            # first line in headers, overcoming it with the except logic
            age = int(age)
            if age > 32: older_then_32.append((name, dep))
        except ValueError:
            # addind the headers to the new csv
            modified_data.append(line)
            continue
        modified_data.append(line)
        # given the locations are sorted as the original data structure
        # adding the location iteratively
        loc = locations[i]
        line[-1] = loc

    return older_then_32, modified_data


def main():
    path = r'C:\Users\nikol\Desktop\csv\csv_example'
    with open(f'{path}.csv', 'r') as f:
        data = csv.reader(f)
        # unpacking the output into variables.

        older_then_32, modified = handle_data(data)
        print(older_then_32)

    new_file_path = f'{path}_modified'
    with open(f'{new_file_path}.csv', 'w') as f:
        writer = csv.writer(f)
        writer.writerows(modified)

if __name__ == '__main__':
    main()
    
