import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# Your email credentials
sender_address = 'nikolnovikov99@gmail.com'
sender_pass = '123456789'
receiver_address = 'example@gmail.com'

# Setup the MIME
message = MIMEMultipart()
message['From'] = sender_address
message['To'] = receiver_address
message['Subject'] = 'A test mail sent by Python. It has an attachment.'   # The subject line

# The body and the attachments for the mail
mail_content = "Hello, this is a test email sent from a Python script. Check it out!"
message.attach(MIMEText(mail_content, 'plain'))

# Create SMTP session for sending the mail
try:
    session = smtplib.SMTP('smtp.gmail.com', 587) # use gmail with port
    session.starttls() # enable security
    session.login(sender_address, sender_pass, ) # login with mail_id and password
    text = message.as_string()
    session.sendmail(sender_address, receiver_address, text)
                     
    session.quit()
    print('Mail Sent Successfully')
except Exception as e:
    print('Failed to send email')
    print(e)                   
